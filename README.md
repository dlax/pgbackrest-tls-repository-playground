# Playground for pgBackRest TLS server feature

This repository provides a Docker compose setup for testing and playing with
pgBackRest TLS server. It aims at illustrating how things work (the *servers*
and *clients* in particular), which part of the configuration is useful for
why, and it tries to decouple things as much as possible in order to avoid the
bias of having everything set up on the same machine.

The setup consists of 3 types of services:

1. several Postgres database servers (`pg15`, `pg14`), configured for
   archiving with pgbackrest, each using their own stanza
2. a pgBackRest `repository`, running a TLS server through `pgbackrest server`
3. a `pgbackrest` (TLS) server running alongside (but outside) the Postgres
   services with access to `$PGDATA`; this service (process) is *common* to all
   database servers

Communication happens as follows:

* WAL archiving goes from the `pg*` host to the `repository` (through
  `archive_command = pgbackrest archive-push`)
* backups are run from `repository`, thus pulling data from the `pgbackrest`
  service (not Postgres hosts) with client command `pgbackrest backup`
* `pgbackrest restore` (client) commands, emitted from the `pg*` host to
  the `repository`

Hosts running `pgbackrest server` (i.e. the `repository` and the service
alongside Postgres hosts) are *servers* as they accept requests from
*clients*; clients are simply `pgbackrest` commands such as `archive-push`,
`backup` or `restore`. Even though these client commands can be run from hosts
running a pgBackRest TLS server, there is no communication between servers.

## pgBackRest (and PostgreSQL) configuration

### Database hosts

Postgres nodes only run client `pgbackrest` commands (typically `archive-push`
and possibly `restore`). WAL archiving is configured normally as:

    archive_mode = on
    archive_command = 'pgbackrest --stanza=<stanza> archive-push %p'

And the pgBackRest configuration consists in:

*   A `[global]` section:

    ```ini
    [global]
    repo1-host=repository
    repo1-host-port=8433
    repo1-host-type=tls
    repo1-host-config=/srv/pgbackrest/server.conf
    repo1-host-cert-file=/etc/pgbackrest/ssl/postgres.crt
    repo1-host-key-file=/etc/pgbackrest/ssl/postgres.key
    repo1-host-ca-file=/etc/pgbackrest/ssl/ca.crt
    ```

    The remote repository is referenced there; notice the `repo1-host-config`
    item which declares the location of the configuration file *on the remote
    host*, when using a non-default value.

    The SSL CA certificate, referenced in `repo1-host-ca-file`, is common to
    the infrastructure while `repo1-host-{cert,key}-file` are owned by each
    host (not shared).

*   a `[<STANZA>]` section with `pg1-path` and `pg1-port` (if needed) defined.

In order to run `pgbackrest` client commands, the stanza needs to specified;
typically, we export a `PGBACKREST_STANZA` environment variable for that.

### The `pgbackrest` service

This service runs alongside Postgres nodes; it has access to their `$PGDATA`.
It only runs the `pgbackrest server` command with the following configuration:

```ini
[global]
tls-server-address=*
tls-server-cert-file=/opt/pgbackrest/ssl/pgbackrest.crt
tls-server-key-file=/opt/pgbackrest/ssl/pgbackrest.key
tls-server-ca-file=/opt/pgbackrest/ssl/ca.crt
tls-server-auth=repository=*
tls-server-port=8434
```

SSL certificate files are installed similarly as for other hosts (the CA is
common, other files are host-specific).

The `tls-server-auth` option indicates that the `repository` (as identified by
it common name) is allowed to communicate with this server; all stanzas (`=*`)
are allowed since this service is common to all Postgres servers and their
stanza.

The configuration is installed in a non-default location, at
`/opt/pgbackrest/pgbackrest.conf`.

### The repository host

This host also runs a `pgbackrest server` process, with the following
*server* configuration (installed at `/srv/pgbackrest/server.conf`):

```ini
[global]
repo1-path=/var/lib/backups

tls-server-address=*
tls-server-cert-file=/srv/pgbackrest/ssl/repository.crt
tls-server-key-file=/srv/pgbackrest/ssl/repository.key
tls-server-ca-file=/srv/pgbackrest/ssl/ca.crt
tls-server-auth=pg15=fst
tls-server-auth=pg14=snd
tls-server-port=8433
```

Options `tls-server-auth` lists Postgres nodes as allowed to communicate with
this server; each only for its stanza.

On this repository host, we also install a *client* configuration:

```ini
[global]
repo1-path=/var/lib/backups
start-fast=y

[fst]
pg1-host=pgbackrest
pg1-host-port=8434
pg1-host-config=/opt/pgbackrest/pgbackrest.conf
pg1-path=/var/lib/postgresql/15/main
pg1-host-type=tls
pg1-host-cert-file=/srv/pgbackrest/ssl/backuper.crt
pg1-host-key-file=/srv/pgbackrest/ssl/backuper.key
pg1-host-ca-file=/srv/pgbackrest/ssl/ca.crt

[snd]
pg1-host=pgbackrest
pg1-host-port=8434
pg1-host-config=/opt/pgbackrest/pgbackrest.conf
pg1-path=/var/lib/postgresql/14/main
pg1-port=5433
pg1-host-type=tls
pg1-host-cert-file=/srv/pgbackrest/ssl/backuper.crt
pg1-host-key-file=/srv/pgbackrest/ssl/backuper.key
pg1-host-ca-file=/srv/pgbackrest/ssl/ca.crt
```

This configuration is typically used by `pgbackrest backup` command, but not
by `pgbackrest server`.

In stanza configurations, `pg1-host` items reference the `pgbackrest` server
*not Postgres nodes*. Also note the `pg1-host-config` option, accounting for
the use of a non-default configuration file location on the `pgbackrest`
server.

## Build, run and play

First create and install certificates, then build Docker images and start all
services:

```console
$ make certs
$ docker-compose build
$ docker-compose up -d
```

Then check that pgBackRest TLS servers are reachable from each others through
`pgbackrest server-ping`:

```console
$ docker-compose exec repository pgbackrest server-ping --tls-server-address=pgbackrest --tls-server-port=8434
INFO: server-ping command begin 2.43: --config=/srv/pgbackrest/client.conf --exec-id=8-337b7298 --log-level-console=off --log-level-file=off --log-level-stderr=info --tls-server-address=pgbackrest --tls-server-port=8434
INFO: server-ping command end: completed successfully (16ms)
$ docker-compose exec pg15 pgbackrest server-ping --tls-server-address=repository --tls-server-port=8433
INFO: server-ping command begin 2.43: --exec-id=16-413e5fd9 --log-level-console=off --log-level-file=off --log-level-stderr=detail --tls-server-address=repository --tls-server-port=8433
DETAIL: statistics: {"socket.client":{"total":1},"socket.session":{"total":1},"tls.client":{"total":1},"tls.session":{"total":1}}
INFO: server-ping command end: completed successfully (8ms)
$ docker-compose exec pg14 pgbackrest server-ping --tls-server-address=repository --tls-server-port=8433
INFO: server-ping command begin 2.43: --exec-id=15-150570b4 --log-level-console=off --log-level-file=off --log-level-stderr=detail --tls-server-address=repository --tls-server-port=8433
DETAIL: statistics: {"socket.client":{"total":1},"socket.session":{"total":1},"tls.client":{"total":1},"tls.session":{"total":1}}
INFO: server-ping command end: completed successfully (13ms)
```

create the stanzas (on the repository host):

```console
$ docker-compose exec repository pgbackrest stanza-create --stanza=fst
INFO: stanza-create command begin 2.43: --config=/srv/pgbackrest/client.conf --exec-id=17-297ac6a2 --log-level-console=off --log-level-file=off --log-level-stderr=info --pg1-host=pgbackrest --pg1-host-ca-file=/srv/pgbackrest/ssl/ca.crt --pg1-host-cert-file=/srv/pgbackrest/ssl/repository.crt --pg1-host-config=/opt/pgbackrest/pgbackrest.conf --pg1-host-key-file=/srv/pgbackrest/ssl/repository.key --pg1-host-port=8434 --pg1-host-type=tls --pg1-path=/var/lib/postgresql/15/main --repo1-path=/var/lib/backups --stanza=fst
INFO: stanza-create for stanza 'fst' on repo1
INFO: stanza-create command end: completed successfully (681ms)
$ docker-compose exec repository pgbackrest stanza-create --stanza=snd
INFO: stanza-create command begin 2.43: --config=/srv/pgbackrest/client.conf --exec-id=23-82fa7c58 --log-level-console=off --log-level-file=off --log-level-stderr=info --pg1-host=pgbackrest --pg1-host-ca-file=/srv/pgbackrest/ssl/ca.crt --pg1-host-cert-file=/srv/pgbackrest/ssl/repository.crt --pg1-host-config=/opt/pgbackrest/pgbackrest.conf --pg1-host-key-file=/srv/pgbackrest/ssl/repository.key --pg1-host-port=8434 --pg1-host-type=tls --pg1-path=/var/lib/postgresql/14/main --pg1-port=5433 --repo1-path=/var/lib/backups --stanza=snd
INFO: stanza-create for stanza 'snd' on repo1
INFO: stanza-create command end: completed successfully (654ms)
```

and finally run `pgbackrest check` commands on Postgres nodes and on the
repository host, for each stanza:

```console
$ docker-compose exec pg15 pgbackrest check
INFO: check command begin 2.43: --exec-id=25-8c46c5b3 --log-level-console=off --log-level-file=off --log-level-stderr=detail --pg1-path=/var/lib/postgresql/15/main --pg1-port=5432 --repo1-host=repository --repo1-host-ca-file=/etc/pgbackrest/ssl/ca.crt --repo1-host-cert-file=/etc/pgbackrest/ssl/postgres.crt --repo1-host-config=/srv/pgbackrest/server.conf --repo1-host-key-file=/etc/pgbackrest/ssl/postgres.key --repo1-host-port=8433 --repo1-host-type=tls --stanza=fst
INFO: check repo1 configuration (primary)
INFO: check repo1 archive for WAL (primary)
INFO: WAL segment 000000010000000000000001 successfully archived to '/var/lib/backups/archive/fst/15-1/0000000100000000/000000010000000000000001-e339136732e02ea66da9ae4e78c1543e96d709f4.gz' on repo1
DETAIL: statistics: {"socket.client":{"total":1},"socket.session":{"total":1},"tls.client":{"total":1},"tls.session":{"total":1}}
INFO: check command end: completed successfully (1232ms)
$ docker-compose exec pg14 pgbackrest check
INFO: check command begin 2.43: --exec-id=23-80153ad9 --log-level-console=off --log-level-file=off --log-level-stderr=detail --pg1-path=/var/lib/postgresql/14/main --pg1-port=5433 --repo1-host=repository --repo1-host-ca-file=/etc/pgbackrest/ssl/ca.crt --repo1-host-cert-file=/etc/pgbackrest/ssl/postgres.crt --repo1-host-config=/srv/pgbackrest/server.conf --repo1-host-key-file=/etc/pgbackrest/ssl/postgres.key --repo1-host-port=8433 --repo1-host-type=tls --stanza=snd
INFO: check repo1 configuration (primary)
INFO: check repo1 archive for WAL (primary)
INFO: WAL segment 000000010000000000000001 successfully archived to '/var/lib/backups/archive/snd/14-1/0000000100000000/000000010000000000000001-6af3907d65510f97c527600991528821ca361383.gz' on repo1
DETAIL: statistics: {"socket.client":{"total":1},"socket.session":{"total":1},"tls.client":{"total":1},"tls.session":{"total":1}}
INFO: check command end: completed successfully (1232ms)
$ docker-compose exec repository pgbackrest check --stanza=fst --no-archive-check --archive-timeout=5
INFO: check command begin 2.43: --no-archive-check --archive-timeout=5 --config=/srv/pgbackrest/client.conf --exec-id=35-9a6d270a --log-level-console=off --log-level-file=off --log-level-stderr=info --pg1-host=pgbackrest --pg1-host-ca-file=/srv/pgbackrest/ssl/ca.crt --pg1-host-cert-file=/srv/pgbackrest/ssl/repository.crt --pg1-host-config=/opt/pgbackrest/pgbackrest.conf --pg1-host-key-file=/srv/pgbackrest/ssl/repository.key --pg1-host-port=8434 --pg1-host-type=tls --pg1-path=/var/lib/postgresql/15/main --repo1-path=/var/lib/backups --stanza=fst
INFO: check repo1 configuration (primary)
INFO: check repo1 archive for WAL (primary)
INFO: WAL segment 000000010000000000000002 successfully archived to '/var/lib/backups/archive/fst/15-1/0000000100000000/000000010000000000000002-8697fdcc56e641de520152c1c52f19cfdce21de7.gz' on repo1
INFO: check command end: completed successfully (1035ms)
$ docker-compose exec repository pgbackrest check --stanza=snd --no-archive-check --archive-timeout=5
INFO: check command begin 2.43: --no-archive-check --archive-timeout=5 --config=/srv/pgbackrest/client.conf --exec-id=44-5d65191c --log-level-console=off --log-level-file=off --log-level-stderr=info --pg1-host=pgbackrest --pg1-host-ca-file=/srv/pgbackrest/ssl/ca.crt --pg1-host-cert-file=/srv/pgbackrest/ssl/backuper.crt --pg1-host-config=/opt/pgbackrest/pgbackrest.conf --pg1-host-key-file=/srv/pgbackrest/ssl/backuper.key --pg1-host-port=8434 --pg1-host-type=tls --pg1-path=/var/lib/postgresql/14/main --pg1-port=5433 --repo1-path=/var/lib/backups --stanza=snd
INFO: check repo1 configuration (primary)
INFO: check repo1 archive for WAL (primary)
INFO: WAL segment 000000010000000000000002 successfully archived to '/var/lib/backups/archive/snd/14-1/0000000100000000/000000010000000000000002-b88fc77b841fcfc76466714c0154f29ea416ad7b.gz' on repo1
INFO: check command end: completed successfully (1032ms)
```

Run a pgbench, to see if archiving works:

    $ make bench

## Backup

Creating a backup is done on the repository host, e.g. for stanza `fst`:

```
$ docker-compose exec repository pgbackrest --stanza=fst backup
INFO: backup command begin 2.43: --config=/srv/pgbackrest/client.conf --exec-id=52-e9ff3e22 --log-level-console=off --log-level-file=off --log-level-stderr=info --pg1-host=pgbackrest --pg1-host-ca-file=/srv/pgbackrest/ssl/ca.crt --pg1-host-cert-file=/srv/pgbackrest/ssl/repository.crt --pg1-host-config=/opt/pgbackrest/pgbackrest.conf --pg1-host-key-file=/srv/pgbackrest/ssl/repository.key --pg1-host-port=8434 --pg1-host-type=tls --pg1-path=/var/lib/postgresql/15/main --repo1-path=/var/lib/backups --repo1-retention-full=2 --stanza=fst --start-fast
WARN: no prior backup exists, incr backup has been changed to full
INFO: execute non-exclusive backup start: backup begins after the requested immediate checkpoint completes
INFO: backup start archive = 000000010000000000000004, lsn = 0/4000028
INFO: check archive for prior segment 000000010000000000000003
INFO: execute non-exclusive backup stop and wait for all WAL segments to archive
INFO: backup stop archive = 000000010000000000000004, lsn = 0/4000138
INFO: check archive for segment(s) 000000010000000000000004:000000010000000000000004
INFO: new backup label = 20230102-094324F
INFO: full backup size = 22.0MB, file total = 961
INFO: backup command end: completed successfully (6094ms)
INFO: expire command begin 2.43: --config=/srv/pgbackrest/client.conf --exec-id=52-e9ff3e22 --log-level-console=off --log-level-file=off --log-level-stderr=info --repo1-path=/var/lib/backups --repo1-retention-full=2 --stanza=fst
INFO: expire command end: completed successfully (31ms)
```

## Restore

In order to test restore, let's first simulate a damage to Postgres data:

    $ docker-compose exec pg15 rm /var/lib/postgresql/15/main/global/pg_control
    $ docker-compose restart pg15
    [...]
    pg15_1    | but could not open file "/var/lib/postgresql/15/main/global/pg_control": No such file or directory
    [...]

Then request a `restore` *from* this Postgres host (by running an ephemeral
container with `$PGDATA` etc.):

```console
$ docker-compose run pg15 pgbackrest restore --delta
INFO: restore command begin 2.43: --delta --exec-id=1-7645f591 --log-level-console=off --log-level-file=off --log-level-stderr=detail --pg1-path=/var/lib/postgresql/15/main --repo1-host=repository --repo1-host-ca-file=/etc/pgbackrest/ssl/ca.crt --repo1-host-cert-file=/etc/pgbackrest/ssl/postgres.crt --repo1-host-config=/srv/pgbackrest/server.conf --repo1-host-key-file=/etc/pgbackrest/ssl/postgres.key --repo1-host-port=8433 --repo1-host-type=tls --stanza=fst
INFO: repo1: restore backup set 20230102-094324F, recovery will start at 2023-01-02 09:43:24
DETAIL: check '/var/lib/postgresql/15/main' exists
INFO: remove invalid files/links/paths from '/var/lib/postgresql/15/main'
DETAIL: remove invalid file '/var/lib/postgresql/15/main/base/1/pg_internal.init'
[...]
DETAIL: remove invalid file '/var/lib/postgresql/15/main/postmaster.opts'
DETAIL: restore file /var/lib/postgresql/15/main/base/5/1255 - exists and matches backup (768KB, 3.41%) checksum 9f62b5a76515e5bb514bf399de4cdd84395ed7d1
[...]
DETAIL: restore file /var/lib/postgresql/15/main/base/1/13371 - exists and is zero size (0B, 100.00%)
INFO: write updated /var/lib/postgresql/15/main/postgresql.auto.conf
DETAIL: sync path '/var/lib/postgresql/15/main'
[...]
DETAIL: sync path '/var/lib/postgresql/15/main/pg_xact'
INFO: restore global/pg_control (performed last to ensure aborted restores cannot be started)
DETAIL: sync path '/var/lib/postgresql/15/main/global'
INFO: restore size = 22.0MB, file total = 961
DETAIL: statistics: {"socket.client":{"total":1},"socket.session":{"total":1},"tls.client":{"total":1},"tls.session":{"total":1}}
INFO: restore command end: completed successfully (477ms)
$ docker-compose start pg15
```

## Pitfalls

On clients of a `pgbackrest server`, it seems needed to specify the path to
configuration file *on the server* in the client configuration: typically
`pg-host-config` and `repo-host-config` (though the latter might not be
mandatory).

In this distributed setup, if the repository host server is down (i.e. the
`pgbackrest server` command is not running), WAL archiving from the database
host blocks. This, in particular, prevents `postgres` from stopping in any
other modes that `immediate`.

The TLS server does not output any log, apart from the startup message.

## References

* <https://pgbackrest.org/user-guide-rhel.html#repo-host>
* <https://www.enterprisedb.com/docs/supported-open-source/pgbackrest/09-tls_server/>
* <https://pgstef.github.io/2022/02/21/pgbackrest_tls_server.html>

<!--
   vim: spelllang=en spell
   -->
