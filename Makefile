certs:
	$(MAKE) -C .certs
	cp .certs/ca.crt .certs/pg1*.crt .certs/pg1*.key postgres/certs/.
	cp .certs/ca.crt .certs/pgbackrest.crt .certs/pgbackrest.key pgbackrest/certs/.
	cp .certs/ca.crt .certs/repository.crt .certs/repository.key repository/certs/.
	cp .certs/ca.crt .certs/backuper.crt .certs/backuper.key repository/certs/.

ping:
	docker-compose exec repository pgbackrest server-ping --tls-server-address=pgbackrest --tls-server-port=8434
	docker-compose exec pg15 pgbackrest server-ping --tls-server-address=repository --tls-server-port=8433
	docker-compose exec pg14 pgbackrest server-ping --tls-server-address=repository --tls-server-port=8433

stanzas:
	docker-compose exec repository pgbackrest stanza-create --stanza=fst
	docker-compose exec repository pgbackrest stanza-create --stanza=snd

check:
	docker-compose exec pg15 pgbackrest check
	docker-compose exec pg14 pgbackrest check
	docker-compose exec repository pgbackrest check --stanza=fst --no-archive-check --archive-timeout=5
	docker-compose exec repository pgbackrest check --stanza=snd --no-archive-check --archive-timeout=5

bench:
	docker-compose exec pg15 createdb bench
	docker-compose exec pg15 pgbench -i bench

backup:
	docker-compose exec repository pgbackrest --stanza=fst backup
	docker-compose exec repository pgbackrest --stanza=snd backup

damage:
	docker-compose exec pg15 rm /var/lib/postgresql/15/main/global/pg_control
	docker-compose exec pg14 rm /var/lib/postgresql/14/main/global/pg_control
	docker-compose restart pg15 pg14

restore:
	docker-compose run pg15 pgbackrest restore --delta
	docker-compose run pg14 pgbackrest restore --delta
	docker-compose restart pg15 pg14

clean:
	-rm postgres/certs/*
	-rm pgbackrest/certs/*
	-rm repository/certs/*
	$(MAKE) -C .certs clean
